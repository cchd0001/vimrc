" 插件管理工具 ----
" ----------------------- Vundle 安装插件 ---------------------------
set nocompatible              " be iMproved, required
set backspace=indent,eol,start
filetype off                  " required
"
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

"------------------------------------ MiniExploer  Start 
"Install from https://github.com/vim-scripts/minibufexpl.vim
Plugin 'vim-scripts/minibufexpl.vim' " 安装上面的小导航栏 ， 记录打开过的文件。
let g:miniBufExplMapWindowNavVim = 1
let g:miniBufExplMapWindowNavArrows = 1
let g:miniBufExplMapCTabSwitchBufs = 1
let g:miniBufExplModSelTarget = 1
let g:bufExplorerMaxHeight=30
let g:miniBufExplorerMoreThanOne=0
map <F1> :MBEbp<CR>
map <F2> :MBEbn<CR>
"------------------------------------ MiniExploer End
"
"------------------------------------ NERDTree Start 
"Install from https://github.com/vim-scripts/The-NERD-tree
Plugin 'vim-scripts/The-NERD-tree' "安装文件树

map <F3> :NERDTreeMirror<CR>
map <F3> :NERDTreeToggle<CR>
let NERDTreeWinPos  = "right"
" Use r to flush NERD tree.
"------------------------------------ NERDTree End
"
"------------------------------------ TarBar Start
"Install from https://github.com/vim-scripts/Tagbar
Plugin 'vim-scripts/Tagbar'
nmap <silent> <F4> :TagbarToggle<CR>
let g:tagbar_width = 30 
let g:tagbar_left = 1 
"------------------------------------ Tarbar End
"
"------------------------------------ YouCompleteMe Start
Plugin 'Valloric/YouCompleteMe'
"配置默认的ycm_extra_conf.py  
"let g:ycm_global_ycm_extra_conf = '~/.vim/YCM/ycm_extra_conf.py'
"打开vim时不再询问是否加载ycm_extra_conf.py配置  
let g:ycm_confirm_extra_conf=0
" 出错提示符号配置
let g:ycm_error_symbol = '>>'
"Syntastic配置   
let g:Syntastic_check_on_open=1 
" 禁用ALT+* 的快捷键先
if has('gui_running') 
    set guioptions-=m
else 
    set <M-f>=f
    set <M-g>=f
    set <M-d>=d
    set <M-i>=i
    set <M-p>=p
    set <M-t>=t
endif
" 跳转还是ctags 好
"ALT+d 跳转到声明或者定义(快但是不精确)
"nmap <M-d> :YcmCompleter GoToImprecise<CR>
"上面的命令替代了下面的两个.
"nmap <M-f> :YcmCompleter GoToDefinition<CR>
"nmap <M-g> :YcmCompleter GoToDeclaration<CR>
"ALT+f 跳转到定义 --  目前不支持
"nmap <M-f> :YcmCompleter GoToImplementationElseDeclaration<CR>
"ALT+i 跳转到头文件
nmap <M-i> :YcmCompleter GoToInclude<CR>
"ALT+p 跳转到基类
"nmap <M-p> :YcmCompleter GetParent<CR>
"ALT+p 跳转到类型
nmap <M-t> :YcmCompleter GetType<CR>
"------------------------------------ YouCompleteMe End

"------------------------------------ Vim-Go Start
Plugin 'fatih/vim-go' "golang 的基本工具.
"------------------------------------ Vim-Go Start
"------------------------------------ The-NERD-Commenter Start
Plugin  'The-NERD-Commenter' "注释代码工具.
"------------------------------------ The-NERD-Commenter End

"------------------------------------ Vim-airline
Plugin  'vim-airline/vim-airline' " 美化状态栏的工具.
Plugin 'vim-airline/vim-airline-themes' "对应的主题包.
let g:airline_theme="dark" 

" 关闭状态显示空白符号计数,这个对我用处不大"
let g:airline#extensions#whitespace#enabled = 0
let g:airline#extensions#whitespace#symbol = '!'
"------------------------------------ Vim-airline
"------------------------------------ Ctrlp
Plugin 'ctrlp.vim'
"------------------------------------ Ctrlp
"
"" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
"
" ----------------------- 显示相关 ---------------------------
" 显示行号
set number
" 背景颜色
set background=dark
" 开启检测语法 。
syntax enable
" 设置配色方案
colorscheme desert
" 设置特殊符号的显示方式
set lcs=tab:▸\ ,trail:·,eol:¬,nbsp:_
" 显示所有的字符
set list
" 字体 字号
set guifont=Consolas\ 18
" ----------------------- 编码解码 ， 文件格式相关 -----------
" 编码格式 utf8 - nobomb
set encoding=utf-8 nobomb
" 文件格式
set fileformat=unix

"------------------------ 特殊处理TAB，对齐相关 --------------
" /t 的显示宽度
set tabstop=4
" 用空格代替制表符 ,在特定时刻请禁止这个选项 ( 比如编辑Makefile )。
" 这个选项不会将已经存在的\t转化为空格。仅仅是添加新的\t的时候会自动改成添加空格。
set expandtab
" 使用TAB 按键的时候进行以4为单位的边界对齐
set softtabstop=4
" 缩进宽度4
set shiftwidth=4
" 自动集成上一行的缩进
set autoindent
" 打开自动缩进
set smartindent
" 支持 C 语言的关键字自动缩进
set cindent


"------------------------ 选中，标记相关 --------------
" 选中高亮
set hlsearch


"------------------------ 备份，缓存相关 --------------
" 没有备份文件 .
set nobackup
" 没有swap 文件 ， 这样就可以打开多个vim 编辑相同文件。
set noswapfile
" 没有写备份文件 .
set nowritebackup


"------------------------ clewn debug 相关 --------------
"add break point
nmap <silent> <C-B> :nbkey C-B<CR>
"clear break point
nmap <silent> <C-E> :nbkey C-E<CR>
"next source line, skipping all function calls
nmap <silent> <F10> :nbkey C-N<CR>
"step into
nmap <silent> <F11> :nbkey S-S<CR>
"step out
nmap <silent> <F12> :nbkey S-F<CR>
"continue
nmap <silent> <F5> :nbkey S-C<CR>

""--------------------------- Mark.vim 自定义颜色
"hi MarkWord7  ctermbg=Cyan     ctermfg=White  guibg=#8CCBEA    guifg=White
"hi MarkWord8  ctermbg=Green    ctermfg=White  guibg=#A4E57E    guifg=White
"hi MarkWord9  ctermbg=Yellow   ctermfg=White  guibg=#FFDB72    guifg=White
"hi MarkWord10  ctermbg=Red      ctermfg=White  guibg=#FF7272    guifg=White
"hi MarkWord11  ctermbg=Magenta  ctermfg=White  guibg=#FFB3FF    guifg=White
"hi MarkWord12  ctermbg=Blue     ctermfg=White  guibg=#9999FF    guifg=White
""--------------------------  回退支持-----
set undofile                " Save undo's after file closes
set undodir=$HOME/.vim/undo " where to save undo histories
set undolevels=1000         " How many undos
set undoreload=100000        " number of lines to save for undo

"-------------------------- Encode --------
set fileencodings=utf-8,ucs-bom,gb18030,gbk,gb2312,cp936
set termencoding=utf-8
set encoding=utf-8
"-------------------------- Ctags ---------
set  cscopetag

